/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.
*/

	db.users.insertMany([
			{
				firstName : "Alfred",
				lastName : "Scott",
				email : "leeferd@mail.com",
				password : "l33",
				isAdmin : false
			},
			{
				firstName : "Chinsea",
				lastName : "Lee",
				email : "chin@mail.com",
				password : "s334",
				isAdmin : false
			},
			{
				firstName : "Vybz",
				lastName : "Kartel",
				email : "vybz@mail.com",
				password : "jamaica",
				isAdmin : false
			},
			{
				firstName : "Grace",
				lastName : "Hamilton",
				email : "grace@mail.com",
				password : "spanishtown",
				isAdmin : false
			},
			{
				firstName : "Samantha",
				lastName : "Gonsalves",
				email : "samantha@mail.com",
				password : "kingston",
				isAdmin : false
			}
		])

/*

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 
*/
		db.courses.insertMany([
				{
					name : "Math",
					price : "5000",
					isActive : false
				},
				{
					name : "English",
					price : "7000",
					isActive : false
				},
				{
					name : "Science",
					price : "6000",
					isActive : false
				}
			
			])


/*
//Read

	>> Find all regular/non-admin users.
*/

		db.users.find({isAdmin : false})
/*

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

*/
		db.users.updateOne({}, {$set: {isAdmin : true}})
		db.courses.updateOne({name : "English"}, {$set : {isActive: true}})


/*
//Delete

	>> Delete all inactive courses
*/
		db.courses.deleteMany({isActive : false})

/*
//Add all of your query/commands here in activity.js

*/